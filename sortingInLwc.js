import { LightningElement , wire, track} from 'lwc';
import getRecords from '@salesforce/apex/SortingExample.getRecords';
const columns = [
    {
        label: 'Name',
        fieldName: 'Name',
        sortable: "true"
    },
    {
        label: 'CloseDate',
        fieldName: 'CloseDate',
        type: 'date',
        sortable: "true"
    }, {
        label: 'StageName',
        fieldName: 'StageName',
        type: 'picklist',
        sortable: "true"
    },
];
export default class SortingInLwc extends LightningElement {
    @track data;
    @track columns = columns;
    @track sortBy;
    @track sortDirection;

    @wire(getRecords)
    Opportunities(result) {
        if (result.data) {
            this.data = result.data;
            this.error = undefined;

        } else if (result.error) {
            this.error = result.error;
            this.data = undefined;
        }
    }

    handleSorting(event) {
       
        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.sort(event.detail.fieldName, event.detail.sortDirection);
    }

    sort(fieldname, direction) {
        let data = JSON.parse(JSON.stringify(this.data));
        let keyValue = (a) => {
            return a[fieldname];
        };

        let isReverse = direction === 'asc' ? 1: -1; 
        data.sort((Value1, Value2) => {
            Value1 = keyValue(Value1) ? keyValue(Value1) : ''; 
            Value2 = keyValue(Value2) ? keyValue(Value2) : '';
            return isReverse * ((Value1 > Value2) - (Value2 > Value1));
        });
        this.data = data;

    }
}