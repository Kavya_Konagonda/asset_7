/**
 * @description       : 
 * @author            : kavya.konagonda@mtxb2b.com
 * @group             : 
 * @last modified on  : 11-30-2020
 * @last modified by  : kavya.konagonda@mtxb2b.com
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   11-30-2020   kavya.konagonda@mtxb2b.com   Initial Version
**/
public with sharing class SortingExample {
    @AuraEnabled(Cacheable = true)
    public static List<Opportunity> getRecords(){
        return [SELECT Id,Name,CloseDate,StageName FROM Opportunity limit 100  ];
    }
}
